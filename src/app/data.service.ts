import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor(private http:HttpClient) { }

  getUsers(){
    console.log('hi')
    // console.log(user)
    return this.http.get("https://jsonplaceholder.typicode.com/users");
  }

  getId(id:any){
    console.log('hi')
    // console.log(user)
    return this.http.get(`https://jsonplaceholder.typicode.com/users/${id}`);
  }
  
  
}
