import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'routerProject';
    
  userObject={};
  display(e:Event)
  {
    this.userObject=e;
    console.log(this.userObject)
  }
}
