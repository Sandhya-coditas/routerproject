import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './details/about/about.component';
import { CompanyComponent } from './details/company/company.component';
import { UserComponent } from './home/user.component';

const routes: Routes = [
  // {path:'home',component:HomeComponent}
  {
    path: '',
    component: UserComponent,
    children:[
      {path:':id',
      component:UserComponent},
      {path:'company/:id',
      component:CompanyComponent},

    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
