import { Component, OnInit,Output,EventEmitter } from '@angular/core';
import { DataService } from 'src/app/data.service';
@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  // [x: string]: any;

  users!:any[];
  selectedUser:any={};
  @Output() displayEvent=new EventEmitter();

  constructor(private dataService:DataService){}
  ngOnInit():void{
    console.log('bfcascjasdsccn');
    this.dataService.getUsers().subscribe({
      next:(response:any)=>{
        this.users=response;
        console.log(this.users)
      }

    });
  }
  selectUser(user:any){
    this.displayEvent.emit(user);}
  }
  // goToUserDetails(id:number) {
  //   this.router.navigate(['/user-details', id]);
  // }
  

