import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserRoutingModule } from './user-routing.module';
import { UserComponent } from './home/user.component';

import { AboutComponent } from './details/about/about.component';
import { CompanyComponent } from './details/company/company.component';


@NgModule({
  declarations: [
    UserComponent,
 
    AboutComponent,
    CompanyComponent
  ],
  imports: [
    CommonModule,
    UserRoutingModule
  ],
  bootstrap: [UserComponent]
})
export class UserModule { }
