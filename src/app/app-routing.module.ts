import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductComponent } from './modules/products/product/product.component';
import { UserComponent  } from './modules/user/home/user.component';

const routes: Routes = [
  {path:'user',
  loadChildren: ()=>
    import('./modules/user/user.module').then(m=>m.UserModule)
  } ,
  {path:'product',component:ProductComponent},
  {path:'',redirectTo:'home',pathMatch:'full'},
  // {path:'**',component:PageNotFoundComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
